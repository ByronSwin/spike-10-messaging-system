#include "Component.h"

using namespace std;

/*
	The component virtual class. This is used to signify and manage traits that entities might have. 
	It has ientifiers, can recieve messages and can return a description

	@param ids for this component
*/
Component::Component(vector<string> ids)
: IObject(ids)
{
}


Component::~Component()
{
}

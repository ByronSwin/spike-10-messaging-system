#include "ComponentLoader.h"

using namespace std;

/*
	ComponentLoader class. Loads an entity and all its components through an in file stream
*/
ComponentLoader::ComponentLoader()
{
}

/*
	Loads an entity and returns it (using the passed in stream). 

	Small amount of recursion. If an entity has an inventory, calls itself to load the entities in that inventory

	@param ifstream& the in-file stream
	@returns Entity the result entity
*/
Entity* ComponentLoader::LoadEntity(ifstream& inStream)
{
	vector<string> newIds = {};
	string name = "";
	string desc = "";
	int numComp;
	string currLine;

	getline(inStream, currLine);

	newIds = SplitString(currLine);

	getline(inStream, name);
	getline(inStream, desc);

	Entity* result = new Entity(newIds, name, desc);
	getline(inStream, currLine);

	numComp = atoi(currLine.c_str());

	for (int i = 0; i < numComp; i++)
	{
		getline(inStream, currLine);

		if (currLine.compare("CARRY") == 0)
		{
			result->AddComponent(new CarryComponent());
		}
		if (currLine.compare("WEAPON") == 0)
		{
			getline(inStream, currLine);
			int chance = atoi(currLine.c_str());
			getline(inStream, currLine);
			int damage = atoi(currLine.c_str());
			
			result->AddComponent(new WeaponComponent(chance, damage));
		}
		if (currLine.compare("LOCK") == 0)
		{
			getline(inStream, currLine);
			string lockVal = currLine;
			getline(inStream, currLine);
			string pathVal = currLine;

			result->AddComponent(new LockComponent(lockVal, pathVal));
		}
		if (currLine.compare("INVENTORY") == 0)
		{
			result->AddComponent(new InventoryComponent());
			getline(inStream, currLine);

			InventoryComponent* resultInv = dynamic_cast<InventoryComponent*>
				(result->GetComponent("INVENTORY"));

			int numberOfItems = atoi(currLine.c_str());
			for (int i = 0; i < numberOfItems; i++)
			{
				resultInv->AddEntity(LoadEntity(inStream));
			}
		}
	}

	cout << "Entity loaded..." << endl;

	return result;
}

/*
	Splits a string by spaces (similar to command processor)
	
	@param input string to format
	@returns vector<string> formatted string
*/
vector<string> ComponentLoader::SplitString(string input)
{
	string next;
	vector<string> result;

	for (string::const_iterator it = input.begin(); it != input.end(); it++)
	{
		if (*it == ' ')
		{
			if (!next.empty())
			{
				result.push_back(next);
				next.clear();
			}
		}
		else
		{
			next += *it;
		}
	}
	if (!next.empty())
	{
		result.push_back(next);
	}

	return result;
}

ComponentLoader::~ComponentLoader()
{
}

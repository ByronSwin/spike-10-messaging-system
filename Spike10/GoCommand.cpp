#include "GoCommand.h"

using namespace std;


/*
	The go command, inheriting from command. This command specifies a location the user wants to travel to

	@param ids identifiers for the go command
*/
GoCommand::GoCommand(vector<string> ids)
:Command(ids)
{
	_outId = "";
}

/*
	Checks that the input means correct formating. If it does, set the out location id as the third word.
	
	@param input the split input
*/
void GoCommand::Update(vector<string> input)
{
	if (input.size() == 3)
	{
		_outId = input.at(2);
	}
	else
	{
		_outId = "NOWHERE";
	}
}

/*
	Returns the out path id

	@return a string for the outid
*/
string GoCommand::GetPathId()
{
	return _outId;
}


GoCommand::~GoCommand()
{
}

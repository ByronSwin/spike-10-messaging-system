#pragma once

#include <iostream>

#include "IObject.h"
#include "Command.h"

class State : public IObject
{
public:
	
	virtual void Handle(Command* cmd) = 0;
	virtual void Display() = 0;
	virtual std::string GetInput() = 0;
	~State();
protected:
	State(std::vector<std::string> ids);
};


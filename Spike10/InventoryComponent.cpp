#include "InventoryComponent.h"
#include "Entity.h"


using namespace std;


/*
	The inventory component, inherits from component.

	This component allows a entity to have a list of entities (i.e. an inventory). This handles basic managment of the inventory and can check
	if it has an entity, can get an entity, can add and take entities
*/
InventoryComponent::InventoryComponent()
:Component({ "INVENTORY" })
{
	
}

/*
	Adds an entity to this inventory

	@param toAdd the pointer to the entity to add
*/
void InventoryComponent::AddEntity(Entity* toAdd)
{
	_inventory.push_back(toAdd);
}

/*
	Checks if this inventory has an etity with a matching identifier. 

	@param toCheck string identifier for entity being looked for

	@returns true if there is a matching identity, else, false.
*/
bool InventoryComponent::HasEntity(string toCheck)
{
	bool result = false;

	for (Entity* e : _inventory)
	{
		if (e->AreYou(toCheck))
		{
			result = true;
			break;
		}
	}

	return result;
}

/*
	Returns the description of this component, including a list of the contained entities.

	@returns string that is the complete component description
*/
string InventoryComponent::CompDesc()
{
	string result = "";

	for (Entity* e : _inventory)
	{
		result += "\n\t";
		result += e->Name();
		result += "\t";
		result += e->FirstId();
	}

	return result;
}

/*
	Takes an entity from this inventory and returns it. Removes the matching entity from this inventory (note, if none found, returns NULL)

	@returns Entity with matching identifier, else NULL
*/
Entity* InventoryComponent::TakeEntity(string toTake)
{ 
	for (vector<Entity*>::const_iterator it = _inventory.begin(); it != _inventory.end(); ++it)
	{
		if ((*it)->AreYou(toTake))
		{
			Entity* temp = (*it);
			_inventory.erase(it);
			return temp;
		}
	}

	return NULL;
}

/*
	Gets an entity by name. Does NOT remove it from this inventory. If no matching entity, returns NULL

	@returns Entity with matching identifier, else NULL
*/
Entity* InventoryComponent::GetEntity(string toTake)
{
	for (vector<Entity*>::const_iterator it = _inventory.begin(); it != _inventory.end(); ++it)
	{
		if ((*it)->AreYou(toTake))
		{
			return (*it);
		}
	}

	return NULL;
}

/*
	A get for the inventory component

	@returns this inventory
*/
vector<Entity*> InventoryComponent::GetInv()
{
	return _inventory;
}

InventoryComponent::~InventoryComponent()
{
}

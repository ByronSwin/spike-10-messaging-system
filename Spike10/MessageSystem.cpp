#include "MessageSystem.h"

using namespace std;


static map<std::string, std::vector<IObject*>> _subscribers;


/**
	Subscribes the IObject to the inputed subject. Also adds subscriber to ANNOUCEMENT if they are not already part of it

	@param subscriber the IObject thats subscribing
	@param subject the string that is the subject of subscription.
*/
void MessageSystem::Subscribe(IObject* subscriber, string subject)
{

	if (!(find(_subscribers[subject].begin(),
		_subscribers[subject].end(),
		subscriber
		) != _subscribers[subject].end()))
	{
		_subscribers[subject].push_back(subscriber);
	}

	if (!(find(_subscribers["ANNOUCEMENT"].begin(),
		_subscribers["ANNOUCEMENT"].end(),
		subscriber
		) != _subscribers["ANNOUCEMENT"].end()))
	{
		_subscribers["ANNOUCEMENT"].push_back(subscriber);
	}
}

/**
	Sends the inputted message to the applicable parties

	@param toSend the message to be sent

*/
void MessageSystem::Send(Message* toSend)
{
	if (toSend->To() == "BLACKBOARD")
	{
		Blackboard::Add(toSend);
	}
	else
	{
		for (map<string, vector<IObject*>>::const_iterator targets = _subscribers.begin(); targets != _subscribers.end(); targets++)
		{
			if (toSend->To().compare( targets->first) == 0)
			{
				for (auto & currObject : targets->second)
				{
					currObject->Recieve(toSend);
				}
				break;
			}
		}
	}
}


/**

		Access a message based on the inputed subject

		@param Accessor the subject to access from the blackboard

		@returns returns the accessed message
*/
Message* MessageSystem::Access(string accessor)
{
	return Blackboard::Access(accessor);
}

MessageSystem::~MessageSystem()
{
}

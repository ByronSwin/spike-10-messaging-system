#pragma once

#include <map>
#include <vector>
#include <string>

#include "Message.h"
#include "Blackboard.h"
#include "IObject.h"

class MessageSystem
{
public:
	
	~MessageSystem();

	 static void Send(Message* toSend);
	 static void Subscribe(IObject* subscriber, std::string subject );

	static Message* Access(std::string accesor);

};


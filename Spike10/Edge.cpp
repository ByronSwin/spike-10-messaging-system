#include "Edge.h"

using namespace std;

/*
	Edge class, used to create connections between locations. Uses identifiers and has a pointer to it's out location

	@param ids identifiers for this path
	@param out the out location* for this path
*/
Edge::Edge(vector<string> ids, Location* out)
: IObject(ids)
{
	_out = out;
}

/*
	@returns the pointer to the out location
*/
Location* Edge::getOut()
{
	return _out;
}

Edge::~Edge()
{
}

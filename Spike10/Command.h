#pragma once

#include "IObject.h"


class Command : public IObject
{
public:
	
	~Command();

	virtual void Update(std::vector<std::string> input);

	protected:
	Command(std::vector<std::string> ids);
};


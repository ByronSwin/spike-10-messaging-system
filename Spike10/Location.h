#pragma once

#include "Entity.h"
#include "Message.h"


class Location : public Entity
{
public:
	Location(std::vector<std::string> ids, std::string name, std::string desc);

	std::string LongDesc();

	virtual void Recieve(Message* toRec);

	bool PathLocked(std::string pathId);

	~Location();
};


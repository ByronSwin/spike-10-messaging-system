#include "StartCommand.h"

using namespace std;


/*
	Start command, inheriting from state command. This command is specifically for the adventure select state, as it needs to only change state after the game has loaded

	@param ids ids for the start command
*/
StartCommand::StartCommand(vector<string> ids)
: StateCommand(ids)
{
	_state = "ADVENTURESELECT";
	_adventure = "";
}

/*
	Handles the formated user input, looking for a title for the adventure

	@param input the formatted split input
*/
void StartCommand::Update(vector<string> input)
{
	if (input.size() > 1)
	{
		if (input.at(1).compare("BACK") == 0)
		{
			_state = "PREVIOUS";
		}
		else if (input.at(1).compare("STARTTHEGAME") == 0)
		{
			_state = "GAMEPLAY";
		}
		else
		{
			_adventure = input.at(1);
			
		}
	}
	else
	{
		_state == "PREVIOUS";
	}
 
}


/*
	Getter for the adventure string

	@return string that is adventure name.
*/
string StartCommand::Adventure()
{
	return _adventure;
}

StartCommand::~StartCommand()
{
}

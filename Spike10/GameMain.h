#pragma once

#include <typeinfo>
#include <string>

#include "CommandProcessor.h"
#include "ContextController.h"

class GameMain
{
public:
	GameMain();
	~GameMain();

	void run();

private:
	ContextController* _Controller;
	CommandProcessor* _CmdProcessor;
};


#include "LookCommand.h"


using namespace std;

/*
	Look command, inherits from command. This command is used to find out what the user wants to look at. 
	The constructor sets intial values.

	@param ids Look command ids
*/
LookCommand::LookCommand(vector<string> ids)
: Command(ids)
{
	_errMsg = "NOERROR";
	_atId = "";
	_inId = "";
}

/*
	Update overide for the look command. Checks first that the size matches one of the acceptable sizes of the array

	Depending on the vector size, checks that non-identifier words are acceptable. if so, updates relevant variables. Else, updates the error message

	@param the formatted split input
*/
void LookCommand::Update(vector<string> input)
{
	_errMsg = "NOERROR";
	_atId = "";
	_inId = "";

	if (input.size() == 4)
	{ 
		if (input.at(2).compare("AT") == 0)
		{
			_atId = input.at(3);
			_inId = "AROUND";
		}
		else 
		{  
			_errMsg = "Look command invalid structure\n";
		}
	}
	else if (input.size() == 6)
	{
		if (input.at(2).compare("AT") == 0 && input.at(4).compare("IN") == 0)
		{
			_atId = input.at(3);
			_inId = input.at(5);
		}
		else
		{
			_errMsg = "Look command invalid structure\n";
		}

	}
	else if (input.size() == 3)
	{
		if (input.at(2).compare("AROUND") == 0)
		{
			_atId = "AROUND";
		}
		else
		{
			_errMsg = "Look at what?\n";
		}
	}
	else if (input.size() == 2)
	{
		_errMsg = "Look at what?\n";
	}
	else
	{
		_errMsg = "Look command size incorrect\n";
	}
}

/*
	Getter for the at identifier
	@returns string at identifier
*/
string LookCommand::getAtId()
{
	return _atId;
}

/*
	Getter for the error message
	@returns string that is the error message
*/
string LookCommand::getErrMsg()
{
	return _errMsg;
}

/*
	Getter for the in identifier
	@returns string in identifier
*/
string LookCommand::getInId()
{
	return _inId;
}

LookCommand::~LookCommand()
{

}

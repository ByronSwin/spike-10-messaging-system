#pragma once

#include <string>
#include "IObject.h"

class Component : public IObject
{
public:
	Component(std::vector<std::string> ids);
	virtual std::string CompDesc() = 0;
	~Component();
};


#pragma once

#include <map>
#include "IObject.h"


#include "MessageSystem.h"
class Component;

class Entity : public IObject
{
public:
	Entity(std::vector<std::string> ids, std::string name, std::string desc);
	~Entity();

	virtual void Recieve(Message* toRec) override;

	virtual void AddComponent(Component* newComponent);
	virtual void RemoveComponent(Component* toRem);
	virtual bool HasComponent(std::string compId);
	Component* GetComponent(std::string toGet);

	std::string Name();
	std::string Desc();
	virtual std::string LongDesc();

protected:
	std::map<std::string, Component*> _components;
	std::string _desc;
	std::string _name;
};


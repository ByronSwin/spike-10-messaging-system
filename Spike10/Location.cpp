#include "Location.h"
#include "InventoryComponent.h"
#include "LockComponent.h"

using namespace std;

/*
	Location class, inherits from entity. All locations have an inventory component. They can also check if a path identifier is currently locked/

	@param ids ids for this location
	@param name string for this location
	@param desc string description for this location
*/
Location::Location(vector<string> ids, string name, string desc)
: Entity(ids, name, desc)
{
	AddComponent(new InventoryComponent());
}

/*
	Overrider for the recieve method. Locations do not pass a message to their components
*/
void Location::Recieve(Message* toRec)
{
	
}

/*
	Method to check if the path identifer path is locked. Searches it's inventory for any entities with a lock component
	and then checks to see if it is currently locked.

	@returns true if an entity locks that path, else false
*/
bool Location::PathLocked(string pathId)
{
	bool result = false;
	if (_components.count("(INVENTORY)") == 1)
	{
		InventoryComponent* inv = dynamic_cast<InventoryComponent*>(_components["(INVENTORY)"]);
		for (Entity* e : inv->GetInv())
		{
			if (e->HasComponent("LOCK"))
			{
				LockComponent* lock = dynamic_cast<LockComponent*>(e->GetComponent("LOCK"));
				if (lock->GetPathVal().compare(pathId) == 0 &&
					lock->isLocked())
				{
					result = true;
					break;
				}
			}
		}
		
	}

	return result;
}

/*
	Returns the long description for this location, including a description of its inventory component

	@returns string full description
*/
string Location::LongDesc()
{
	string result = "\n\t" + _name;
	result = "\n" + _desc;
	if (_components.count("(INVENTORY)") == 1)
	{
		result += "\nYou can see:";
		InventoryComponent* inv = dynamic_cast<InventoryComponent*>(_components["(INVENTORY)"]);
		result += inv->CompDesc();
	}

	return result;
}

Location::~Location()
{
}

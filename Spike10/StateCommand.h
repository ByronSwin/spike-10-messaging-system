#pragma once
#include "Command.h"

class StateCommand : public Command
{
public:
	StateCommand(std::vector<std::string> ids);
	~StateCommand();
	void Update(std::vector<std::string> input);

	std::string State();

protected:
	std::string _state;
};


#include "Message.h"

using namespace std;

/*
	The message class, used by IObjects (though primarily components and entities) to both send and recieve.

	Each message has a sender (id of who sent), a to (who to send it too) a subject(what is the message about) and 
	data (what is the relevant information given the subject).

	All of these fields have getters.
	
	@param sender the sender of this message
	@param to target of the message
	@param subject what the message is about
	@param data relevant information

*/
Message::Message(string sender, string to, string subject, string data)
{
	
	_sender = sender;
	_to = to;
	_subject = subject;
	_data = data;
}

/*
	@returns the sender id
*/
string Message::Sender()
{
	return _sender;
}

/*
	@returns the target's id
*/
string Message::To()
{
	return _to;
}

/*
	@returns the subject of the message
*/
string Message::Subject()
{
	return _subject;
}

/*
	@returns the data of the message
*/
string Message::Data()
{
	return _data;
}


Message::~Message()
{

}
